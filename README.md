# Ubuntu 16.04 [![build status](https://gitlab.com/nvidia/videosdk/badges/master/build.svg)](https://gitlab.com/nvidia/videosdk/commits/ubuntu16.04)

## Video Codec SDK 8.2 (with CUDA 9.2)
- `8.2-ubuntu16.04` [(*ubuntu16.04/Dockerfile*)](https://gitlab.com/nvidia/videosdk/blob/master/ubuntu16.04/Dockerfile)
- `8.2-ubuntu18.04` [(*ubuntu18.04/Dockerfile*)](https://gitlab.com/nvidia/videosdk/blob/master/ubuntu18.04/Dockerfile)
- `8.2-centos7` [(*centos7/Dockerfile*)](https://gitlab.com/nvidia/videosdk/blob/master/centos7/Dockerfile)
